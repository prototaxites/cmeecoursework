#!/usr/bin/python

#r'^abc[ab]+\s\t\d' at the start of the string, match the characters abc, then at least one
# or more of either a or b, then match a space, a tab, and any digit
 
 example1 = "abcababab 	5"
 match = re.search(r'^abc[ab]+\s\t\d',example1)
 print match.group()

#r'^\d{1,2}\/\d{1,2}\/\d{4}$' - at the start of the string, match a digit at least 1 time but
# not more than 2, then match a slash, a digit at least 1 time but not more than 2, a slash,
# and then match exactly four digits at the end of the string

example2 = "45/32/1452"
match = re.search(r'^\d{1,2}\/\d{1,2}\/\d{4}$',example2)
print match.group()

# r'\s*[a-zA-Z,\s]+\s*' - match any string of A-Z, a-z, commas and spaces, and the
# spaces surrounding it if present 

example3 = " aaaCf, "
match = re.search(r'\s*[a-zA-Z,\s]+\s*',example3)
print match.group()

##################

# YYYYMMDD

datestr = "19920123"
match = re.search(r'(19|2\d)\d{2}[01]\d[0-3]\d', datestr)
match.group()
