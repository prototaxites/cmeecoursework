#!/usr/bin/python

"""regexs.py - A short demonstration of the use of regular expressions
   and their (expected) outputs on given strings."""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

import re

my_string = "a given string"
# find a space in the string

match = re.search(r'\s', my_string) # r tells python to read he regex in its raw/literal form

print match
# this should print something like
# <_sre.SRE_Match object at 0x93ecdd30>

# now we can see what has matched

match.group()

match = re.search(r's\w*', my_string)

# this should return "string"
match.group()

# NOW AN EXAMPLE OF NO MATCH:
# find a digit in the string
match = re.search(r'\d', my_string)

#this should print 'None'

print match

# Further example

string = 'an example'
match = re.search(r'\w*\s',string) # returns "an "
if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'
	
# Some basic examples
match = re.search(r'\d' , "it takes 2 to tango")
print match.group() #print 2

match = re.search(r'\s\w*\s', 'once upon a time')
match.group() #' upon '

match = re.search(r'\s\w{1,3}\s', 'once upon a time')
match.group() # ' a '

match = re.search(r'\s\w*$', 'once upon a time')
match.group() # ' time'

match = re.search(r'\w*\s\d.*\d', 'take 2 grams of H2O')
match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', 'once upon a time')
match.group() # 'once upon a '

## Note that *, +, and {} are all "greedy":
## They repeat the previous regex token as many times as possible
## As a result, they may match more text than you want

## To make it non-greedy, use ?:
match = re.search(r'^\w*.*?\s', 'once upon a time')
match.group() # 'once '

## To further illustrate greediness, let's try matching an HTML tag:
match = re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group() # '<EM>first</EM>'

# But we just wanted <EM>
# + is a greedy character!

## Instead we can make + "lazy"!
match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group() #'<EM>'

## Moving on from greed and laziness
match = re.search(r'\d*\.?\d*','1432.75+60.22i')
match.group() # '1432.75'

match = re.search(r'\d*\.?\d*','1432+60.22i')
match.group() #'1432'

match = re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() # 'ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', "The bird-shit frog's name is Theloderma asper").group() # ' Theloderma asper'

MyStr = 'Samraat Pawar, s.pawar@imperial.ac.uk, Systems biology and ecological theory'
# without groups
match = re.search(r"[\w\s]*,\s[\w\.@]*,\s[\w\s&]*",MyStr)
match.group()
# now add groups using ()
match = re.search(r"([\w\s]*),\s([\w\.@]*),\s([\w\s&]*)",MyStr)
match.group(0) # 'Samraat Pawar, s.pawar@imperial.ac.uk, Systems biology and ecological theory'
match.group(1) # 'Samraat Pawar'
match.group(2) # 's.pawar@imperial.ac.uk'
match.group(3) # 'Systems biology and ecological theory'
