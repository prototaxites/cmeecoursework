#!/usr/bin/env python
"""align_seqs.py - Reads in a csv file containing two DNA sequences separated
   by a comma, and repeatedly aligns them to find the position where there
   are the greatest number of base matches between the two sequences."""

import csv

# Open a given csv file, then loop through it to pull out the two sequences
# and assign them as variables
f = open('../Data/seqs.csv', 'rb')
csvread = csv.reader(f)
for row in csvread:
	seq1 = row[0]
	seq2 = row[1]
	
f.close()

# Assign the longest sequence to variable seq_long, and the shortest sequence to seq_short
# Similarly, length_long is the length of the longest sequence, and
# length_short that of the shortest
length_long = len(seq1)
length_short = len(seq2)
if length_long >= length_short:
	seq_long = seq1
	seq_short = seq2
else:
	seq_long = seq2
	seq_short = seq1
	length_long, length_short = length_short, length_long # swap the two lengths
	

# Function that computes a score by returning the number of  
# matches starting from an arbitrary startpoint
def calculate_score(seq_long, seq_short, length_long, length_short, startpoint):
	"""calculate_score takes two DNA sequences, and starting from an
	   arbitrary startpoint along the longer sequence, compares each letter
	   of the aligned sequence and counts the number of matches. It then prints
	   this information graphically that shows the alignment of each
	   sequence and the number base matches.
	   
	   Use: calculate_score(long_sequence, short_sequence, long_sequence_length, short_sequence_length, startpoint)
	"""
	# Startpoint is the position along seq_long where we want to begin comparison
	matched = "" # Empty string used to align the short sequence above the large sequence
	score = 0
	for i in range(length_short):
		if (i + startpoint) < length_long: # Don't overrun!
			# If characters match
			if seq_long[i + startpoint] == seq_short[i]:
				matched = matched + "*" # Add a * to matched
				score = score + 1 #Increase the score
			else:
				matched = matched + "-" # Add a - to matched

	# build some formatted output
	print "." * startpoint + matched # Print (startpoint) number of dots, then the *s and -s in matched
	print "." * startpoint + seq_short # Print (startpoint) number of dots, and then the short sequence
	print seq_long
	print score 
	print ""

	return score

# Now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

# Iteratively run calculate_length() for each position along the long sequence,
# then if a result is better than the current best result, overwrite the best result
for i in range(length_long):
	z = calculate_score(seq_long, seq_short, length_long, length_short, i)
	if z > my_best_score:
		my_best_align = "." * i + seq_short
		my_best_score = z

print my_best_align
print seq_long
print "Best score:", my_best_score

# Save the results to a text file for later access.
resultsfile = open("../Results/bestresult.txt", "w")
resultsfile.write(str(my_best_align) + '\n' + str(seq_long) + '\n' + 'Best Score: ' + str(my_best_score) + '\n')
resultsfile.close()

print '\nResults saved in "../Results/bestresult.txt".'
