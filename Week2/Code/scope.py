#!/usr/bin/python

"""scope.py - A small program that demonstrates how variables work inside
   and outside of functions, i.e. global and local variables. Shows how to
   designate a local variable as a global one so that it can be accessed later
   - use sparingly!"""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

## Try this

_a_global = 10

def a_function():
	"""Function with locally defined variables that differ from the 
	   global ones"""
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

a_function()

print "Outside the function, the value is ", _a_global

## Now try this

_a_global = 10

def a_function():
	"""Function with a globally defined variable which changes when
	   the function is run"""
	global _a_global
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

a_function()
print "Outside the function, the value is ", _a_global
