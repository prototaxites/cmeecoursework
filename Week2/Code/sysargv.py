#!/usr/bin/python

"""sysargv.py - Shows basic use of the sys module. Takes any number of
   arguments when run and then prints information about the such as the
   number of arguments, their contents and the script name."""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

import sys
print "Name of the script is: ", sys.argv[0]
print "Number of arguments are: ", len(sys.argv)
print "The arguments are: ", str(sys.argv)
