#!/usr/bin/python
"""lc2.py - A script that sorts through the 1910 Average UK Rainfall 
   (mm) data set to pull out entries that meet certain criteria such as
   rainfall being greater than or smaller than a given number"""
   
__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.

## Create a list called largerainfall, then use a list comprehension to iterate through each tuple in rainall. 
## For each tuple, checks the second value to see if it is >= 100, and if so adds it to largerainfall.
largerainfall = [r for r in rainfall if r[1] >= 100]
print largerainfall
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

## Create a list called smallrainfall, then use a list comprehension to iterate through each tuple in rainall. 
## For each tuple, checks the second value to see if it is <= 50, and if so adds it to smallrainfall.
smallrainfall = [r[0] for r in rainfall if r[1] <= 50]
print smallrainfall

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

largerainfall = [] # Create an empty list called largerainfall
for pair in rainfall: # For every tuple in rainfall,
	if pair[1] >= 100: # Check if the second value is >= 100
		largerainfall.append(pair) # If so, add that tuple to largerainfall
print largerainfall

smallrainfall = [] # Create an empty list called smallrainfall
for pair in rainfall: # For every tuple in rainfall,
	if pair[1] <= 50: # Check if the second value is <= 50
		smallrainfall.append(pair[0]) # If so, add that tuple to smallrainfall
print smallrainfall
