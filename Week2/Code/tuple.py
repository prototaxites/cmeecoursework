#!/usr/bin/python
"""tuple.py - This script loops through the tuple 'birds'. Each tuple in
   birds contains three separate data entries for a bird - latin name,
   common name and body mass. For each bird, this script prints each of
   those data types on a separate line."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehension!

# Loops through each tuple in birds and, for every tuple print each entry
# on a new line along with a prefix stating the type of data that it is
# (latin name, common name, body mass (g)
for bird in birds:
	print 'Latin name: ', bird[0], '\n', 'Common name: ', bird[1], '\n', 'Body mass: ', bird[2], 'g', '\n'
