#!/usr/bin/python

"""using_name.py - A very simple demonstration of how
   if __name__ == "__main__": works when importing the script or running
   it directly."""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

if __name__ == "__main__":
	print "this program is being run by itself!"
else:
	print "i am being imported from another module!"
