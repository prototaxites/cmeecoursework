#!/usr/bin/python
"""boilerplate.py - Description of this program
    you can use several lines
    Docstring comments would go here"""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)' #internal variables
__version__ = '0.0.1'

# imports
import sys # module to interface our program with the OS

# constants can go here

# functions can go here
def main(argv):
	print 'This is a boilerplate' #Indented using one tab
	return 0

if (__name__ == "__main__"): #makes sure "main" function is called from commandline
	status = main(sys.argv)
	sys.exit(status)
