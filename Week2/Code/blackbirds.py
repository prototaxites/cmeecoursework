#!/usr/bin/env python

"""blackbirds.py - an exercise to run repeated regexes over a file.
   Reads in blackbirds.txt, and using regex groups pulls out only the
   information about Kingdom, Phylum and Species for each entry."""
   
__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

print text

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# Regular expression! Uses Python-specific syntax to name groups
# (?P<name>regex) which allows groups to be pulled out by name, which
# is pretty neat, all things considered. 
#
# re.findall() only returns matches within groups, too, which is useful.
my_reg = r'(?P<kingdom>Kingdom\s\w+).*?(?P<phylum>Phylum\s\w+).*?(?P<species>Species\s\w+\s\w+)'
blackbirds = re.findall(my_reg, text)

for item in blackbirds:
	print item[0] + ', ' + item[1] + ', ' + item[2]

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
