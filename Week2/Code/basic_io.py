#!/usr/bin/python

"""basic_io.py - Program that demonstates the basics of reading and writing files,
   using both text files and pickle binary dumps"""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

#################################
# FILE INPUT
#################################

# Open a file for reading
f = open('../Sandbox/test.txt', 'r')
# use "implicit" for loop:
# if the object is a file, python will cycle over lines
for line in f:
	print line, #the "," prevents adding a new line
# close the file
f.close()

# same example, skip blank lines
f = open('../Sandbox/test.txt', 'r')
for line in f:
	if len(line.strip()) > 0:
		print line,
		
f.close()

#################################
# FILE OUTPUT
#################################

# Save the elements of a list to a file
list_to_save = range(100)

f = open('../Sandbox/testout.txt', 'w')
for i in list_to_save:
	f.write(str(i) + '\n') # add a new line at the end
f.close()

#################################
# STORING OBJECTS
#################################
# To save and obejct (even complex) for later use
my_dictionary = {"a key": 10, "another key": 11}

import pickle # module for saving python objects as binary files

f = open('../Sandbox/testp.p', 'wb') ## note the b: accept binary files
pickle.dump(my_dictionary, f)
f.close()

## Load the data
f = open('../Sandbox/testp.p', 'rb')
another_dictionary = pickle.load(f)
f.close()

print another_dictionary
