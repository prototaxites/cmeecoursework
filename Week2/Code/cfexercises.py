#!/usr/bin/python

"""cfexercises.py - Program that demonstrates how to use control flows such as
   if, else, for loops and while loops. Also contains a main function
   that executes some of the functions with example values to demonstrate
   their use"""

__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

#How many times will 'hello' be printed?

import sys

# 1)
for i in range(3, 17):
	print 'hello'
	
# 2)
for j in range(12):
	if j % 3 == 0:
		print 'hello'

# 3)
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 == 3:
		print 'hello'
		
# 4)
z = 0
while z != 15:
	print 'hello'
	z = z+3 #increase counter

# 5)	
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1
	
# What does fooXX do?
	
def foo1(x):
	"""Finds the square root of x"""
	return x ** 0.5

def foo2(x, y):
	"""Prints the bigger number of given numbers x and y"""
	if x > y:
		return x
	return y

def foo3(x, y, z):
	"""Rearranges x,y, and z in order of size"""
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z] #return a list

def foo4(x):
	"""Finds the factorial of x"""
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result
	
def foo5(x):
	"""Recursive function that returns the factorial of x"""
	if x == 1:
		return 1
	return x * foo5(x-1) # Calls this function recursively on x-1 - results in x*(x-1)(x-1)...(1)

def main(argv):
	print foo1(4)
	print foo2(6,127)
	print foo3(47,22,963)
	print foo4(5)
	print foo5(5)
	return 0

if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
