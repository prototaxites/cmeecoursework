#!/usr/bin/python

"""loops.py - demonstrates the use of for and while loops, and the concept
   of infinite loops."""

__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

# For loops in python
for i in range(5):
	print i

my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list:
	print k
	
total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands:
	print total + s
	
# while loops in python
z = 0
while z < 100:
	z = z + 1
	print(z)
	
#creates an infinite loop
b = True
while b:
	print "GERONIMO! infinite loop! ctrl-c to stop!"
