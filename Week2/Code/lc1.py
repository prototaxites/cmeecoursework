#!/usr/bin/python
"""lc1.py - A script that iterates through the tuple 'birds' and
   separates it into three separate lists containing the three data
   types held in birds - latin name, common name, and body mass"""
   
__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively.

## For every tuple 'bn' in birds, add the first element of the tuple to
## binomialname
binomialname = [bn[0] for bn in birds]
print binomialname

## For every tuple 'cn' in birds, add the second element of the tuple to 
## commonname
commonname = [cn[1] for cn in birds]
print commonname

## For every tuple 'bm' in birds, add the third element of the tuple to
## bodymass
bodymass = [bm[2] for bm in birds]
print bodymass

# (2) Now do the same using conventional loops (you can choose to do 
# this before 1 !). 

## Create an empty list called binomialname, then loop through the tuple 
## birds. For every tuple in birds, pull out the first element and add it
## to binomialname.
binomialname = []
for bn in birds:
	binomialname.append(bn[0])
print binomialname
	
## Create an empty list called commonname, then loop through the tuple
## birds. For every tuple in birds, pull out the second element and add
## it to  commonname.
commonname = []
for cn in birds:
	commonname.append(cn[1])
print commonname

## Create an empty list called bodymass, then loop through the tuple
## birds. For every tuple in birds, pull out the third element and add
## it to bodymass.
bodymass = []
for bm in birds:
	bodymass.append(bm[2])
print bodymass
