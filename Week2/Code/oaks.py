#!/usr/bin/python

"""oaks.py - demonstrates the use of list comprehensions using a simple
   example - searching a small dataset of tree species and pulling out
   oak species. Compares the use of list comprehensions with more
   lengthy but perhaps more clear for loops."""
   
__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

taxa = [ 'Quercus robur',
		 'Fraxinus excelsior',
		 'Pinus sylvestris',
		 'Quercus cerris',
		 'Quercus petraea',
	   ]
	   
	   
def is_an_oak(name):
	"""Reads a given string, converts it to lower case and returns it if
	   it starts with "quercus " - i.e. if it is an oak"""
	return name.lower().startswith('quercus ')
	
## Using for loops
oaks_loops = set()
for taxon in taxa:
		if is_an_oak(taxon):
			oaks_loops.add(taxon)
print oaks_loops

## Using list comprehensions
oaks_lc = set([t for t in taxa if is_an_oak(t)])
print oaks_lc

## Get names un upper case using for loops
oaks_loops = set()
for taxon in taxa:
	if is_an_oak(taxon):
		oaks_loops.add(taxon.upper())
print oaks_loops

## Get names in upper case using list comprehensions
oaks_lc = set([t.upper() for t in taxa if is_an_oak(t)])
print oaks_lc
