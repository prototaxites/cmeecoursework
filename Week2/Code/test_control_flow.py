#!/usr/bin/python
"""test_control_flow.py - demonstrates the basics of unit testing.
   By putting some unit tests in the docstring of a funtion, plus their
   expected outcome, the function can  be tested using the doctest.testmod()
   function."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)' #internal variables
__version__ = '0.0.1'

# imports
import sys # module to interface our program with the OS
import doctest

def even_or_odd(x=0): # if not specified, x should take value 0
	"""Find whether a number x is even or odd
	
	>>> even_or_odd(10)
	'10 is even!'
	
	>>> even_or_odd(5)
	'5 is odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is odd!'
	
	in case of negative number, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is even!'
	
	"""
	if x % 2 == 0:
		return "%d is even!" % x
	return "%d is odd!" % x
	
#~ def main(argv):
	#~ # sys.exit("don't want to do this right now!")
	#~ print even_or_odd(22)
	#~ print even_or_odd(33)
	#~ return 0

#~ if (__name__ == "__main__"): #makes sure "main" function is called from commandline
	#~ status = main(sys.argv)

doctest.testmod() # runs unit testing as set out in the docstrings
