#!/usr/bin/python
"""dictionary.py - Loops through the list of tuples 'taxa' and converts
   it to a dictionary where the family names are keys and each key
   contains a set with each species in that family, e.g.
   'Chiroptera' : set(['Myotis lucifugus'])"""
   
__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# Creates empty dictionary called taxa_dic. Loops through taxa, and for 
# every tuple uses the dict.setdefault(key, default) function. 
# dict.setdefault looks through a dictionary for a key, and if it 
# doesn't find it, it creates one with the default value (in this case
# an empty set). It then adds the species in question to the set for the
# key in question.
taxa_dic = {}
for species, family in taxa:
	taxa_dic.setdefault(family, set()).add(species)

print taxa_dic
