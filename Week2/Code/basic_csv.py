#!/usr/bin/python

"""basic_csv.py - Basic program that reads and writes some .csv files."""

__author__ = "Jim Downie (james.downie15@imperial.ac.uk)"
__version__ = "0.0.1"

import csv


# Read a file containing:
# 'Species', 'Infraorder', 'Family', 'Distribution', 'Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv', 'rb')
csvread = csv.reader(f)
temp = [] # open an empty list
for row in csvread:
	temp.append(tuple(row)) # appends each row of the csv as a tuple to temp - allows the data to be manipulated after closing the file
	print row
	print "The species is", row[0]

f.close()

f = open('../Sandbox/testcsv.csv','rb')
g = open('../Sandbox/bodymass.csv','wb')

csvread = csv.reader(f) # function that iteratively reads through a csv file
csvwrite = csv.writer(g) # as above, but writes data iteratively to a csv
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]]) # write rows 0 and 4 (species and body mass) to a new file

f.close()
g.close()
