# R Practical 2

# Using the colon to define ranges
1:10
-4:4
12:3

# Or we can do it using seq(), which provides more fine-grain control
seq(from = 1, to = 10)
seq(from = 1, to = 20, by = 2)
seq(from = 1, to = 3, length = 10)

# We can repeat sequences a certain number of times, or repeat items in a sequence
rep(x = 1:4, times = 2)
rep(x = 1:4, each = 2)
rep(x = 1:4, times = 1:4)

# Changing a character vector to a factor
myCharacterVector <- c("low", "low", "low", "low", "high", "high", "high", "high")
myFactor <- as.factor(myCharacterVector)
str(myFactor)

# using gl() [GenerateLevels] to create factors
myFactor <- gl(n = 2, k = 4)
print(myFactor)

# We can add labels
myFactor <- gl(n = 2, k = 4, labels = c("low", "high"))
print(myFactor)

# And set lengths
myFactor <- gl(n = 2, k = 2, length = 8, labels = c("low", "high"))
print(myFactor)

# create some data vectors
myNumericVector <- c(1.3, 2.5, 1.9, 3.4, 5.6, 1.4, 3.1, 2.9)
myCharacterVector <- c("low", "low", "low", "low", "high", "high", "high", "high")
myLogicalVector <- c(TRUE, TRUE, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE)
# join them into columns of a data frame
myData <- data.frame(num = myNumericVector, char = myCharacterVector, logical = myLogicalVector)
str(myData)
print(myData)

# Don't turn the character vector into a factor
myData <- data.frame(num = myNumericVector, char = myCharacterVector, logical = myLogicalVector, stringsAsFactors = FALSE)
str(myData)

## Reading files
factories <- read.csv("../Data/factories.csv")
str(factories)

summary(factories)

rate
# The column rate is contained within factories, and can be accessed with $
factories$rate
# We can get the rate column using indices
factories[1:5, 3]
factories$rate[1:5]

# It's easier to find stuff by name, though, and means that the code should still work if the data changes
factories$factory == "Sheffield"
factories$rate[factories$factory == "Sheffield"]
factories$treatment == "C"
factories$rate[factories$treatment == "C"]

# Add a new data column for country
factories$country <- "England"
# change the label to Wales for the factories in
# Gwent and Newport
factories$country[factories$factory == "Gwent"] <- "Wales"
factories$country[factories$factory == "Newport"] <- "Wales"
str(factories)
factories$country <- as.factor(factories$country)

# Now we can pull out just the Welsh data
welshData <- subset(factories, subset = country =="Wales")
str(welshData)

welshRates <- subset(factories, subset = country =="Wales", select = rate)
str(welshRates)
