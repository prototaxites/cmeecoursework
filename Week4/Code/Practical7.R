# R practical 7

## Two-way analysis of variance
# Initial data exploration
daphnia <- read.delim("../Data/daphnia.txt")
summary(daphnia)
par(mfrow=c(1, 2))
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)

## Function - calculates the standard error of vector x
seFun <- function(x) {
  sqrt(var(x)/length(x))
}

# Grab some means and standard errors for groups
detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent, FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent, FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))

# Plot a bar chart
par(mfrow = c(2, 1), mar = c(4, 4, 1, 1))
barmids <-barplot(detergentMean, xlab = "Detergent type", ylab = "Population growth rate", ylim = c(0, 5))
arrows(barmids, detergentMean - detergentSEM, barmids, detergentMean + detergentSEM, code = 3, angle = 90)

barMids <- barplot(cloneMean, xlab = "Daphnia clone", ylab = "Population growth rate", ylim = c(0, 5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM, code = 3, angle = 90)

# Linear model
daphniaMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaMod)
summary.lm(daphniaMod)

# Run Tukey's HSD, and plot the graph
daphniaModHSD <- TukeyHSD(daphniaMod)
daphniaModHSD
par(mfrow = c(1, 2), mar = c(4, 8, 1, 1), las = 1)
plot(daphniaModHSD)

## Multiple regression
timber <- read.delim("../Data/timber.txt")
summary(timber)
pairs(timber) # plot all the data against itself
cor(timber) # get r values for each factor pair

# Linear model
timberMod <- lm(volume ~ girth + height, data = timber)
summary.aov(timberMod)
summary(timberMod)

## ANCOVA
plantGrowth <- read.delim("../Data/ipomopsis.txt")
summary(plantGrowth)

# Run linear model
plantMod <- lm(Fruit ~ Root + Grazing, data = plantGrowth)
anova(plantMod)
summary(plantMod)

## Plot graph
plantGrowth$plotChar <- 1 # set up vector for point types
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plantGrowth$plotCol <- "red" # And again for colour types
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"
plot(Fruit ~ Root, data = plantGrowth, ylab = "Fruit production (dry weight, mg)", xlab = "Initial root diameter (mm)", pch = plotChar, col = plotCol)

# Add lines to the graph for the two grazing types, as calculated from the lm function
abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "blue")
