%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PREAMBLE                                                        %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[12pt]{article}

% load some packages
\usepackage{authblk} % Affiliations
\usepackage{setspace} % Double spacing
\usepackage{lineno} % Line numbers
\usepackage[a4paper, margin=2cm]{geometry} % 2cm margins
%\usepackage{siunitx} % Degree symbols and the like
\usepackage[parfill]{parskip} % Lines between paragraphs, no indentation
\usepackage{graphicx} % images
%\usepackage[font=it]{caption} % italic legend captions
\usepackage[section]{placeins} % force floats to remain within their sections

% Harvard-style referencing
\usepackage[comma]{natbib}
\bibliographystyle{agsm}
\setcitestyle{authoryear,open={(},close={)}}

% line numbering
\doublespacing

% sans serif fonts
%\renewcommand{\familydefault}{\sfdefault} % main text
%\usepackage[cm]{sfmath} % math font

% Document header
\title{\textbf{Modelling changes in mycorrhizal communities using a Bayesian network}}
\author[1]{Jim Downie}
\affil[1]{Imperial College London}
\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEXT                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
  \maketitle
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Supervisors                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\centering Supervisors and Committee Members}
\begin{center}
	\textbf{Supervisor:} Martin Bidartondo, Imperial College London, m.bidartondo@imperial.ac.uk
	
	\textbf{Committee member:} David Orme, Imperial College London
	
	\textbf{Committee member:} Veronique Lefebvre, Imperial College London
\end{center}

\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Introduction                                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}
\begin{linenumbers}
Mycorrhiza constitute an important, mostly hidden component of many forest ecosystems. Through the provisioning of hard-to-access nutrients in exchange for carbon, and increasing the resistance of trees to drought, mycorrhiza play an important role in maintaining the health of forest communities \citep{smith_mycorrhizal_2008}. Although much work has been undertaken examining mycorrhizal ecology at the local scale, particularly on interactions between individuals, little is known about broad-scale patterns in mycorrhizal ecology, with a lack of baseline data on patterns of biodiversity, endemism \citep{talbot_endemism_2014}, invasion, or the causative mechanisms of such patterns \citep{lilleskov_can_2007}. Previous research in this area has found a link between increasing atmospheric nitrogen deposition and lower fungal diversity at large scales \citep{cox_nitrogen_2010}, but more work needs to be done to build up a full picture. 

Given the ecological importance of mycorrhiza, and the lack of baseline data required for effective planning regarding their conservation, a Europe-wide study has been set up to examine large-scale trends in mycorrhizal ecology \citep{suz_monitoring_2015}. The experiment makes use of the pre-existing International Co-operative Programme on Assessment and
Monitoring of Air Pollution Effects on Forests (ICP Forests), utilising the environmental data collected at approximately 130 of these sites in conjunction with 300 mycorrhizal samples for genotypic identification from each plot. This is intended to provide a baseline for the assessment of mycorrhizal community health going forward.

To investigate what processes might be driving mycorrhizal diversity at large scales, I propose to build a Bayesian network investigating the interrelations between the environmental variables measured at each plot and mycorrhizal diversity. Bayesian networks are probabilistic models which allow the structure of processes to be represented graphically, allowing causal inferences to be made, for example from changes to the system. This can be important in informing decision making and generating predictions to test qualitative assumptions \citep{department_of_environment_water_heritage__and_the_arts_australian_government_bayesian_2010}.
\end{linenumbers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methods                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Methods}
\begin{linenumbers}
The first step in the project will be to clean up the environmental data. The data has been recorded by numerous people from many organisations and countries; thus it will need to be reorganised so that the data available for each plot is roughly equivalent. This will be done using R in order to produce a dataframe or set of dataframes that are suitable. Once this has been done, an ordination will be performed in order to get a sense of how each species responds to environmental variation. This should demonstrate both how plots differ in terms of environmental variables, and how species composition may vary with the environment.

To determine how each environmental variable might relate to each other within the Bayesian network, linear models will be conducted to determine the type, strength and direction of the relationships between variables. With this understanding, the levels of each variable can then be chosen in order to create a parsimonious model. In terms of the output variables of interest, either a simple measure of species richness or the presence of a functional group \citep{suz_environmental_2014} may be chosen.
\end{linenumbers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Feasibility                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Project Feasibility}
\begin{linenumbers}
This project should be wholly feasible within the given time period. The environmental data associated with each of the plots is already available, and the OTU/taxonomic data is expected to be completed by May. The most complicated parts of the project may be to get the data sorted out into a suitable format, and then to construct the network itself.
\end{linenumbers}

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=0.4]{../Results/gantt.png} 
	\caption{Gantt chart with projected timings for the project.
    \label{fig:gantt}}
\end{figure}

\bibliography{proposal}
\end{document}