library(DiagrammeR)
a <- mermaid("
        gantt
        dateFormat  YYYY-MM-DD
        title Project timeline
        
        section Data analysis
        Prepare / explore data    :        data_1,    2016-04-08, 2016-04-28
        Ordination                :        data_2,    2016-04-21, 7d
        Linear models             :        data_3,    after data_2, 14d
        Build Bayesian network    :        data_4,    after data_3, 40d

        section Research
        Initial reading                    :    res_1,   2016-04-08, 21d
        Write methods for data preparation :    res_2,   after data_1, 3d
        Methods for Ordination             :    res_3,   after data_2, 3d
        Methods for Linear models          :    res_4,   after data_3, 3d
        Methods for Bayesian network       :    res_5,   after data_4, 7d
        Write introduction                 :    res_6,   after res_1, 30d
        Write results                      :    res_7,   after data_4, 21d
        Write discussion                   :    res_8,   after res_7, 30d
        ")

print(a)