#!/bin/sh

pandoc +RTS -K512m -RTS 'Inputs/Seminar Diary.md' \
--to latex --from markdown+autolink_bare_uris+ascii_identifiers+tex_math_single_backslash \
--output 'Seminar Diary.pdf' \
--number-sections \
--chapters \
--highlight-style tango \
--latex-engine pdflatex \
--template 'Config/default.tex'
