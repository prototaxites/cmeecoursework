%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PREAMBLE                                                        %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[11pt]{article}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}

% load some packages
\usepackage{authblk} % Affiliations
\usepackage{setspace} % Double spacing
\usepackage{lineno} % Line numbers
\usepackage[top=1in, bottom=1.25in, left=1.25in, right=1.25in]{geometry} % Get the margins down pat
\usepackage{siunitx} % Degree symbols and the like
\usepackage[parfill]{parskip} % Lines between paragraphs, no indentation
\usepackage{graphicx} % images
\usepackage[font=it]{caption} % italic legend captions
\usepackage[section]{placeins} % force floats to remain within their sections

% Harvard-style referencing
\usepackage[comma]{natbib}
\bibliographystyle{agsm}
\setcitestyle{authoryear,open={(},close={)}}

% line numbering
\doublespacing

% sans serif fonts
\renewcommand{\familydefault}{\sfdefault} % main text
\usepackage[cm]{sfmath} % math font

% Document header
\title{\textbf{Comparison of three Schoolfield temperature response models using non-linear least squares}}
\author[1]{Jim Downie}
\affil[1]{Imperial College London}
\date{}

% word count
\newcommand\wordcount{(\input{count.txt}words)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEXT                                                            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}
  \maketitle

\begin{center}\wordcount\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Introduction                                                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Introduction}
\begin{linenumbers}
Climate change is one of the biggest threats to ecosystem stability and biodiversity. The increase in average global temperatures is likely to affect most ecosystems, causing species range shifts and extinctions. These effects at the individual species level will have large consequences for ecosystem structure and function. For example, marine phytoplankton contribute half of the world's GPP; with climate change increasing the mean temperature of the water, this will effect changes to phytoplankton productivity, phenology, population size and community composition. However, such changes have largely been considered as a result of indirect effects, such as increasing ocean stratification, and not as a function of increasing temperature on the organisms themselves \citep{thomas_global_2012}. As the majority of life is ectothermic, their internal rates of reaction are strongly determined by the external temperature, and thus increasing global mean temperatures is likely to have stong effects on these species' abilities to survive.

In order to determine what direct effects increasing temperatures may have on species persistence, it is important to examine the response curves of some biological trait against temperature. These biological responses can take many forms, including respiration rates, growth rates, and photosynthesis/$CO_2$ assimilation rates. The response curves of these traits against temperature share two features: unimodality, and negative skewness. Unimodality of the response suggests that each curve has a single temperature at which it experiences a maximum value. The negative skewness of the response (i.e. a steeper decline in rate above the optimum temperature than below it) means that organisms are more sensitive to warming than to cooling, which has important consequences when considered in the context of climate change \citep{thomas_global_2012}.

It is possible to model such response curves. A large number of different models have been created to describe such curves. One of the primary such models used, the Schoolfield model, is defined in \citet{schoolfield_non-linear_1981}, and is formulated as follows:

\begin{equation}
B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}{1 + e^{\frac{E_l}{k} (\frac{1}{T_l} - \frac{1}{T})} + e^{\frac{E_h}{k} (\frac{1}{T_h} - \frac{1}{T})}}
\end{equation}

The model is based around Arrhenius' empirical equation, and hence has a basis in absolute reaction rate theory. It assumes that, for all temperatures, the rate of the trait being modelled is controlled by a single rate controlling enzyme, and that at high and low temperatures, this enzyme is reversibly inactivated. The model also assumes a constant total concentration of the enzyme at all temperatures. This model has been derived from a model in an earlier paper \citep{sharpe_reaction_1977}, in order to reduce correlation between parameters to improve its suitability for NLLS fitting, and to ensure that parameters have strong biological interpretations.

The parameters can be interpreted thusly: \textbf{k} is Boltzmann's constant, ($8.617 \times 10^{-5} eV \cdot K^{-1}$), $\mathbf{B_0}$ is the estimated value of the trait  at 10\si\degree C, \textbf{E} is the enthalpy of activation of the reaction, $\mathbf{E_l}$ is the change in enthalpy associated with low temperature inactivation of the model, $\mathbf{E_h}$ is the change in enthalpy associated with high temperature inactivation of the model $\mathbf{T_l}$ is the temperature (in Kelvin) at which half of the enzyme is active and half is inactive due to low temperature, and similarly $\mathbf{T_h}$ is the temperature (in Kelvin) at which half of the enzyme is active and half is inactive due to high temperature.

For many response curves, however, it may be inappropriate to use the Schoolfield model to fit the data, due to weak or undetectable signals of enzyme deactivation in the response curve. In such cases, it may be more appropriate to fit a simplified version of the Schoolfield model, depending on whether it is the low-temperature deactivation or high-temperature deactivation signal that is missing. For models where high temperature signals are missing (TL model), the Schoolfield model can be simplified to:

\begin{equation}
B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}{1 + e^{\frac{E_l}{k} (\frac{1}{T_l} - \frac{1}{T})}}
\end{equation}

Similarly, the Schoolfield model can be simplified for models with missing low temperature signals (TH model) as follows:

\begin{equation}
B = \frac{B_0 e^{\frac{-E}{k} (\frac{1}{T} - \frac{1}{283.15})}}{1 + e^{\frac{E_h}{k} (\frac{1}{T_h} - \frac{1}{T})}}
\end{equation}

To determine whether such mechanistic models are more effective than simple phenomenological explanations, the results can be compared to a simple cubic model which approximates the shape of the temperature response curve, where $C_i$ are parameters and T is the temperature:

\begin{equation}
C = C_0 + C_1T + C_2T^{2} + C_3T^{3}
\end{equation}

In this paper, I aim to determine how well such mechanistic models fit a dataset of thermal responses, and whether they are an improvement over simple phenomenological models. I will also investigate the results of the model fits and how the parameter values found compare to values found by other studies, and any biological interpretations that can be made.

\end{linenumbers}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Methods                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Methods}
\subsubsection*{Data}
\begin{linenumbers}
In order to compare the goodness of fit of each of these models, a large database of temperature response curves containing measurements compiled from many lab experiments across the world. The responses fall largely into the categories of growth rates, respiration rates, and photosynthetic rates, and cover a wide range of taxa including plants and algae, fungi, arthropods, bacteria, unicellular eukaryotes and more. Additional data columns cover a wide range of factors of interest including habitat, tropic group and sample location.

This database initially contained 2,314 unique responses; from these, only responses that had greater than 5 data points were used for model fitting. In order to correct the data for later log transformation, response curves with negative trait values were normalised by subtracting the value of the lowest trait value from each data point, and then adding 0.1 to each value in order to remove zeroes. All temperature values were converted to Kelvin by adding 273.15. As a result of this processing, 1951 response curves were used for model fitting.

\end{linenumbers}
\subsubsection*{Model fitting}
\begin{linenumbers}

It was decided to use non-linear least squares (NLLS) to fit each model due to the non-linearity of the model equations. NLLS is an process that iteratively tries different values for each parameter value in order to minimise the residual sum of squares. In order to do this effectively, it is important to provide reasonable initial estimates for each parameter, a task that is simplified here due to the biological interpretability of each parameter. Parameter boundaries were set in order to maintain a certain level of biological realism; in particular, values for $\mathbf{T_l}$ and $\mathbf{T_h}$ were bounded so that they never overlap, and that $\mathbf{T_l}$ always falls below $T_{opt}$ whilst $\mathbf{T_h}$ always falls above it. This also has the advantage of ensuring that the two simplified models cannot fit to the exact same parameter values, and are therefore modelling different aspects of each temperature response, allowing them to be compared effectively. The parameters were estimated as follows:

\begin{itemize}
\item $\mathbf{B_0}$: for each response curve, the temperature closest to 10\si\degree C was found, and the trait value at this temperature was chosen as $\mathbf{B_0}$. Units: unit of measurement of the trait itself. (Bounds: -10 to the value of the trait with the highest value in the response curve being fitted)
\item $\mathbf{E}$: In order to estimate the activation energy \textbf{E}, a simplified version of the Arrhenius equation ($B = B_0 e^{-\frac{E}{kT}}$) was used. Taking the natural logarithm of the equation yields a linear model ($log(B) = log(B_0) -\frac{E}{kT}$) which was fitted to each response curve in R using the lm() function. This provides an estimate of \textbf{E} as the slope of the equation. Units: electron volts (eV). (Bounds: $1 \times 10^{-16}$ to 10)
\item $\mathbf{E_l}$: ${E_l}$ was assumed to be $\frac{1}{2}E$. Units: electron volts (eV). (Bounds: $1 \times 10^{-16}$ to 10)
\item $\mathbf{E_h}$: ${E_h}$ was assumed to be $E \times 5$. Units: electron volts (eV). (Bounds: $1 \times 10^{-16}$ to 50)
\item $\mathbf{T_l}$: for each response curve, the temperature at which the highest trait value occurred was identified. $\mathbf{T_l}$ was taken to be the temperature halfway between this temperature and the coolest value in the response curve. Units: Kelvin. (Bounds: 250 to $T_{opt}$, the temperature at which the trait value is highest)
\item $\mathbf{T_h}$: Similarly, for each response curve, $\mathbf{T_h}$ was taken to be the temperature halfway between the temperature with the highest trait value and the hottest temperature in the response curve. If the temperature with the highest trait value was the hottest temperature in the response curve, then this temperature itself was picked as $\mathbf{T_h}$. Units: Kelvin. (Bounds: $T_{opt}$ to 400 K)
\end{itemize}

In order to carry out the NLLS fits, a script was written in Python that takes the sorted data and attempts to fit each model for each curve. The trait data for each NLLS fit for the three mechanistic models were transformed using $log(x)$ prior to fitting; this significantly decreased the time for each fit to converge on the correct value. Once the parameter values were estimated, the residuals were calculated again using these parameter values and the untransformed data in order to calculate the true value of the adjusted $R^2$ and RSS. Additionally, the AIC and BIC were calculated, as well as the $\chi ^{2}$ for each fit.

Models were compared using multiple criteria in order to determine which model fit the data best overall. For each of the three measures of fit used (adjusted $R^2$, AIC, and BIC), for each temperature curve, the model with either the lowest AIC/BIC or highest adjusted $R^2$ was determined. The total number of cases where each model was the best fit was then counted for each measure of fit.

\end{linenumbers}
\subsubsection*{Parameter interpretations}
\begin{linenumbers}
In order to determine whether the distributions found for the parameters were reasonable, density curves were plotted for each parameter for each model, and the mode of the distribution was calculated and plotted to the graph.
\end{linenumbers}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Results                                                         %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Results}
\subsubsection*{Fits}
\begin{linenumbers}

To determine whether a model fitted successfully, it was decided to use the arbitrary criterion of adjusted $R^2 > 0.5$, in line with a similar study by \citet{dell_systematic_2011}. Using this criterion, it was found that for the 1951 response curves deemed suitable for fitting, the full model fit 1265 curves, the TL simplified model fit 1594 curves, the TH model fit 1719 curves, and the simple cubic model fit 1889 curves. For each response curve, each model that fitted was plotted to a graph (for an example, see fig. \ref{MTD2703}) and saved in the ../Results/Graphs directory.

\begin{figure}[!ht]
\centering
\includegraphics[scale=1.00]{../Results/Graphs/MTD2703.pdf}
\caption{Temperature response of the growth rate of \textit{Prochlorococcus marinus}, a Cyanobacterium, for which all four models were found to fit.}
\label{MTD2703}
\end{figure}

\end{linenumbers}
\subsubsection*{Model comparison}
\begin{linenumbers}
% latex table generated in R 3.2.3 by xtable 1.8-2 package
% Tue Mar  8 01:18:22 2016
\begin{table}[!h]
\centering
\begin{tabular}{cccc}
  \hline
 & AIC & BIC & adj. $R^2$ \\ 
  \hline
Full & 239 & 233 & 482 \\ 
  TL & 175 & 175 & 139 \\ 
  TH & 389 & 390 & 314 \\ 
  Cubic & 1094 & 1099 & 962 \\ 
   \hline
\end{tabular}
\caption{Number of models selected as the best fit for each of the three measures of fit (AIC, BIC, adjusted $R^2$).} 
\label{counts}
\end{table}


Table \ref{counts} shows the results of the model comparison for each of the three measures of fit. It can be seen that the phenomenological cubic model is the best model for the majority of response curves for each measure. Between the three mechanistic models, the TH model fits the most models best based on AIC and BIC, but based on adjusted $R^2$ the full model has the most number of fits. This is unsurprising, as AIC and BIC penalise model fits based on the number of parameters, whereas $R^2$ will almost always increase with an increasing number of parameters.
\end{linenumbers}
\subsubsection*{Parameter interpretation}
\begin{linenumbers}
% latex table generated in R 3.2.3 by xtable 1.8-2 package
% Tue Mar  8 01:18:22 2016
\begin{table}[!h]
\centering
\begin{tabular}{cccc}
  \hline
 & E & $E_l$ & $E_h$ \\ 
  \hline
Full & 0.628 & 1.184 & 2.501 \\ 
  TL & 0.643 & 1.296 &  \\ 
  TH & 0.565 &  & 1.579 \\ 
   \hline
\end{tabular}
\caption{Mode of the parameter value for each model.} 
\label{modes}
\end{table}


Table \ref{modes} shows the mode of each of the three E parameters and the $B_0$ parameter. The distributions of the activation energy, E, can be seen in Fig. \ref{Egraph}. The distribution is strongly negatively skewed for all three models; a similar result was additionally observed for $E_l$ and $E_h$. The distribution of $B_0$ was even more negatively skewed, with almost all of the points clustered around zero with a very long tail with increasing $B_0$. Only a small fraction of $B_0$ values are negative.

\begin{figure}[!ht]
\centering
\includegraphics[scale=1.00]{../Results/E.pdf}
\caption{Distribution of the activation energy, E, post-fitting for each of the three models. The vertical line shows the position of the mode.}
\label{Egraph}
\end{figure}

\end{linenumbers}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Discussion                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Discussion}
\subsubsection*{Model comparison}
\begin{linenumbers}
From the results, it can be seen that the cubic model is the best fitting model for the majority of response curves. Of the three mechanistic models, the TH model is the best fitting model, followed by the full model (ignoring $R^2$, as it does not penalise models for having higher numbers of parameters). This could be due to a number of factors; as the TH model is intended to be used for models where the signal from low temperatures is weak, or where low temperature data is missing, it may be that the majority of temperature response curves in the dataset fit this description and are thus best fitted by this model compared to the TL model. The two simplified models, considered together, fit more datasets (AIC: 564; BIC: 565) than the full model (AIC: 239; BIC: 233); thus, in cases where the temperature response curve can be seen to be missing either a low or high temperature signature, it may be wise to choose one of these models over the full model.

Although the cubic model is the best fitting model overall, it may be unwise to choose it over the mechanistic models. As it is a phenomenological model, it makes no attempt to explain \textit{why} the observed trait responds to temperature, instead simply describing the relationship observed. By comparison, the Schoolfield models attempt to explain the trait responds to temperature in terms of absolute reaction rate theory \citep{schoolfield_non-linear_1981}, modelling it as the fraction of a single enzyme that is deactivated by high and low temperatures. Particularly with the Schoolfield model, where the parameters are all relatively easily-quantifiable in terms of biology, this means that the temperature response can be considered as the result of an interaction of parts, and the goodness of fit of the model can be considered in terms of the mechanism chosen to describe the response. If the mechanism chosen to describe the response is poor, then the fit of the model will be poor, and vice versa.

Another potential issue with using the cubic model is that of overfitting, where the model explains the random noise in the dataset rather than describing the relationship between the two variables. Particularly because the parameters of the cubic model have no biological meaning, it makes very little sense to constrain the parameters within any bounds. This means that the parameters, and hence the shape of the curve, can take any form within the bounds of the equation, and can hence regularly fit the dataset far better than a constrained mechanistic model where the parameters are confined to biological realities. For example, the intercept parameter of the cubic model ranges between \ensuremath{-4.8526147\times 10^{7}} to \ensuremath{8.3169968\times 10^{7}}, while the parameter for the squared term ranges from \ensuremath{-1708.8297677} to 2845.3285548. This may lead to the cubic model fitting the data far better a lot of the time, but leaves very little information in terms of interpretation and is likely not useful for future predictions. Thus, even though the cubic model appears to be the best fitting model here, it may be wise to discount it and use one of the Schoolfield models instead.

\end{linenumbers}
\subsubsection*{Parameter interpretations}
\begin{linenumbers}
The metabolic theory of ecology (MTE) is an attempt to link the metabolism of individual organisms to broad-scale ecological processes such as energy fluxes through functions of allometric scaling, biochemical kinetics and chemical stoichiometry \citep{brown_toward_2004}. One of the primary equations used in the MTE is:

\begin{equation}
I = i_0 \cdot M^{\frac{3}{4}} \cdot e^{\frac{-E}{kT}}
\end{equation}

where I is the individual metabolic rate, $i_0$ is a normalisation constant, and M is the body mass. The final element, the Boltzmann factor, $e^{\frac{-E}{kT}}$ (as seen previously in the Schoolfield models) is a common factor in the MTE that is used to model the distribution of particles in a system over various possible states. \textbf{E}, the mean activation energy, has been approximated to about 0.65 eV \citep{allen_kinetic_2006, gillooly_effects_2001} in previous studies. A similar result was found for all three models in this study (Table \ref{modes}). The mode for the full Schoolfield model was 0.628, with Q1 0.608, median 0.973, and and Q3 1.68. The distribution of \textbf{E} can be seen in Fig. \ref{Egraph}. Thus, this result provides further evidence for the choice of a number in this range for the mean activation energy when using models of this type.

\end{linenumbers}
\subsubsection*{Model fitting issues}
\begin{linenumbers}
One issue with the parameter interpretations that makes model interpretation difficult are the $\mathbf{B_0}$ values for curves which had negative data points. Because these curves were transformed by subtracting the most negative trait value of the curve from all values in the curve(bringing all values to zero or above), $\mathbf{B_0}$ values estimated from these curves have an associated error that was not accounted for here in this study. Further work here could be to untransform the data post-fitting in order to achieve $\mathbf{B_0}$ values in the same scale as the initial dataset; this would be preferential to removing datasets with negative values, which would simply lead to a loss of data.
\end{linenumbers}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% References                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{linenumbers}
\bibliography{writeup}
\end{linenumbers}
\end{document}
