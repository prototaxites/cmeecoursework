#!/usr/bin/env python
""" This code performs non-linear least squares fitting of different
unimodal functions to experimental thermal response curves."""
__author__ = 'Jim Downie (james.downie15@imperial.ic.ac.uk)'
__version__ = '0.4.0'

import csv, sys, timeit
from math import pi, log
import scipy as sc
from lmfit import Minimizer, minimize, Parameters, Parameter, report_fit, fit_report
from lmfit.models import PolynomialModel
import progressbar as pb

###############################################################################
## Equation functions                                                        ##
###############################################################################

def schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h, model):
	"""Schoolfield model equations - will run the chosen Schoolfield model
	(full, TL or TH) depending on the argument given. Arguments are as follows:

	Temps: An array of temperature values to fit the model over
	B0: The value of the trait value at 283.15 K
	E: The activation energy of the temperature response
	E_l: The low-temperature half-deactivation energy
	E_h: The high-temperature half-deactivation energy
	T_l: The low temperature at which half the enzyme is deactivated
	T_h: The high temperature at which half the enzyme is deactivated
	model: which version of the schoolfield model to run
	"""
	## mad wee formula
	if model == "full":
		function = (B0 * sc.exp((-E/k) * ((1 / Temps) - (1 / 283.15)))) / (1 + sc.exp( (E_l / k) * ((1 / T_l) - (1 / Temps))) + sc.exp((E_h / k) * ( (1 / T_h) - (1 / Temps))))
	if model == "tl":
		function = (B0 * sc.exp((-E/k) * ((1 / Temps) - (1 / 283.15)))) / (1 + sc.exp( (E_l / k) * ((1 / T_l) - (1 / Temps))))
	if model == "th":
		function = (B0 * sc.exp((-E/k) * ((1 / Temps) - (1 / 283.15)))) / (1 + sc.exp( (E_h / k) * ((1 / T_h) - (1 / Temps))))

	## Return log of the output so that it's in the same scale as the actual data
	return function

###############################################################################
## Lmfit functions                                                           ##
###############################################################################

def schoolf_full(params, Temps, TraitVals):
	"""Full Schoolfield model, to be called by schoolfield_model().

	Takes 3 arguments:
	params: Parameters dictionary object from schoolfield_model()
	Temps: an array of temperature vectors
	TraitVals: an array of trait values
	"""
	## Define parameters
	B0 = params['B0'].value
	E = params['E'].value
	E_l = params['E_l'].value
	T_l = params['T_l'].value
	E_h = params['E_h'].value
	T_h = params['T_h'].value

	## Call model
	ModelPred = schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h, "full")
	LogMod = sc.array(map(sc.log, ModelPred), dtype=sc.float64)
	LogTraits = sc.array(map(sc.log, TraitVals), dtype=sc.float64)
	## Return residuals
	return(LogMod - LogTraits)

def schoolf_tl(params, Temps, TraitVals):
	"""Schoolfield TL model, to be called by schoolfield_model()

	Takes 3 arguments:
	params: Parameters dictionary object from schoolfield_model()
	Temps: an array of temperature vectors
	TraitVals: an array of trait values
	"""
	B0 = params['B0'].value
	E = params['E'].value
	E_l = params['E_l'].value
	T_l = params['T_l'].value
	E_h = 0
	T_h = 0

	ModelPred = schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h, "tl")
	LogMod = sc.array(map(sc.log, ModelPred), dtype=sc.float64)
	LogTraits = sc.array(map(sc.log, TraitVals), dtype=sc.float64)
	## Return residuals
	return(LogMod - LogTraits)

def schoolf_th(params, Temps, TraitVals):
	"""Schoolfield TH model, to be called by schoolfield_model()

	Takes 3 arguments:
	params: Parameters dictionary object from schoolfield_model()
	Temps: an array of temperature vectors
	TraitVals: an array of trait values
	"""
	B0 = params['B0'].value
	E = params['E'].value
	E_h = params['E_h'].value
	T_h = params['T_h'].value
	E_l = 0
	T_l = 0

	ModelPred = schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h, "th")
	LogMod = sc.array(map(sc.log, ModelPred), dtype=sc.float64)
	LogTraits = sc.array(map(sc.log, TraitVals), dtype=sc.float64)
	## Return residuals
	return(LogMod - LogTraits)

###############################################################################
## Schoolfield modelling function                                            ##
###############################################################################

def schoolfield_model(dataset, initparams, model):
	"""This function runs NLLS fits on the various Schoolfield models. It accepts
	3 arguments:

	- dataset: The trait values of a single growth curve as an array
	- initparams: The initial parameter values as a named array
	- model: which model to run

	It then sets up the initial parameters, and passes the ones necessary for
	NLLS fitting to the requisite fitting function defined by the model parameter.
	"""
	# Prepare the parameters and their bounds:
	B0_start = initparams['B0']
	E_start = initparams['E']
	E_l_start = initparams['E_l']
	T_l_start = initparams['T_l']
	E_h_start = initparams['E_h']
	T_h_start = initparams['T_h']

	## The dataset containing temperatures and trait values.
	## Log the trait dataset using sc.log1p to do (log(x + 1)) to avoid errors
	TraitVals = dataset['trait']
	Temps = dataset['temperature']

	## Temperature at which the trait value is highest
	## Force to an integer for better fits
	Tmax = int(Temps[sc.argmax(TraitVals)])

	# Define parameters
	params = Parameters()
	params.add('B0', value = B0_start, vary = True, min = -10, max = TraitVals[sc.argmax(TraitVals)])
	params.add('E', value=E_start, vary= True, min=0.0000000000000001, max=10)
	## Only send the correct number of parameters to lmfit for each model
	## Otherwise we will be giving 6 parameters to the simplified model when
	## they only need 4
	if model == "full":
		params.add('E_l', value=E_l_start, vary = True, min=0.0000000000000001, max=10)
		params.add('T_l', value=T_l_start, vary = True, min=250, max=Tmax)
		params.add('E_h', value=E_h_start, vary = True, min=0.0000000000000001, max=50)
		params.add('T_h', value=T_h_start, vary = True, min=Tmax, max=400)
		## Model fit
		out = minimize(schoolf_full, params, args = (Temps, TraitVals), method = "leastsq")
		## Get the residuals for the unlogged data
		res = schoolf_eq(Temps, out.params['B0'].value, out.params['E'].value, out.params['E_l'].value, out.params['T_l'].value, \
		out.params['E_h'].value, out.params['T_h'].value, model) - TraitVals

	if model == "tl":
		params.add('E_l', value=E_l_start, vary = True, min=0.0000000000000001, max=10)
		params.add('T_l', value=T_l_start, vary = True, min=250, max=Tmax)
		out = minimize(schoolf_tl, params, args = (Temps, TraitVals), method = "leastsq")
		res = schoolf_eq(Temps, out.params['B0'].value, out.params['E'].value, out.params['E_l'].value, out.params['T_l'].value, \
		0, 0, model) - TraitVals

	if model == "th":
		params.add('E_h', value=E_h_start, vary = True, min=0.0000000000000001, max=50)
		params.add('T_h', value=T_h_start, vary = True, min=Tmax, max=400)
		out = minimize(schoolf_th, params, args = (Temps, TraitVals), method = "leastsq")
		res = schoolf_eq(Temps, out.params['B0'].value, out.params['E'].value, 0, 0, \
		out.params['E_h'].value, out.params['T_h'].value, model) - TraitVals

	## Get the residual sum of squares
	RSS = sum(res ** 2)
	# Calculates the r squared value to determine how well the model fits the data.
	r_squared_school = 1 - sc.var(res) / sc.var(TraitVals)
	aic = AICrss(out.ndata, out.nvarys, RSS)
	bic = BICrss(out.ndata, out.nvarys, RSS)

	return(out.params, r_squared_school, aic, bic, out.chisqr, out.ndata, out.nvarys)

###############################################################################
## Cubic model                                                               ##
###############################################################################

def CubicModel(dataset):
	""" Runs a phenomenological cubic model on the data. Takes 1 argument,
	a dataset of traits and temperatures.
	"""
	## Define x and y
	x = dataset['temperature']
	y = dataset['trait']

	## Generate a model object with the requisite number of degrees
	model = PolynomialModel(degree = 3)
	## Guess starting values for the parameters
	params = model.guess(y, x=x)

	## Fit the model to the data
	out = model.fit(y, params, x=x)

	## Define some outputs for simplicity
	r_squared_cubic = 1 - out.residual.var() / sc.var(y)
	RSS = sum(out.residual ** 2)
	aic = AICrss(out.ndata, out.nvarys, RSS)
	bic = BICrss(out.ndata, out.nvarys, RSS)

	return(out.params, r_squared_cubic, aic, bic, out.chisqr, out.ndata, out.nvarys)

###############################################################################
## Auxiliary functions                                                       ##
###############################################################################

def AICrss(n, k, rss):
	"""Calculate the Akaike Information Criterion value, using:

	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n * log((2 * pi) / n) + n + 2 + n * log(rss) + 2 * k

def BICrss(n, k, rss):
	"""Calculate the Bayesian Information Criterion value, using:

	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n + n * log(2 * pi) + n * log(rss / n) + (log(n)) * (k + 1)

###############################################################################
## Main code                                                                 ##
###############################################################################

def main(data, paramdata):
	"""Performs fitting to the Gaussian-Gompertz, Schoolfield and Cubic model,
	and returns the best fits as a csv file to ../Results/results.csv"""

	## Start the timer
	start = timeit.default_timer()

	## Read in the data files.
	## data contains the traits and temperatures, and parameterdata contains the initial parameter values.
	data = sc.genfromtxt(data, dtype = None, delimiter = ',', deletechars='"', skip_header=0, names=True, usecols = [1, 2, 3, 4])
	parameterdata = sc.genfromtxt(paramdata, dtype = None, delimiter = ',', deletechars='"', skip_header=0, names=True, usecols = [1, 2, 3, 4, 5, 6, 7, 8, 9])

	# Define the Boltzmann constant (units of eV * K^-1).
	global k
	k = 8.617 * 10 ** (-5)

	#Open the csv file to write the output to.
	results = open("../Results/results.csv", 'w')
	results_csv = csv.writer(results, delimiter=",")

	## Write the column headers to make use in R later easier.
	results_csv.writerow(["ID", "FinalID", "model", "B0", "E", "E_l", "T_l", "E_h", "T_h", "c0", "c1", "c2", "c3", "Rsq", "AIC", "BIC", \
	"Chisq", "Ndata", "Nvarys", "Habitat"])

	## Let's have a progress bar!
	pbar = pb.ProgressBar(widgets=[pb.Percentage(), pb.Bar(marker = "=")], maxval=len(parameterdata) + 1).start()
	## Loop and attempt to run NLLSs for each id.
	for i in range(1, len(parameterdata) + 1):
		## [1:-1] strips quotes
		FinalID = data[data['id'] == i][1][3][1:-1]
		Habitat = parameterdata[parameterdata['id'] == i][0][8][1:-1]
		try:
			## The construct "data[data['id'] == i]" pulls out only the data points for an individual ID.
			full = schoolfield_model(data[data['id'] == i], parameterdata[parameterdata['id'] == i], model = "full")

			results_csv.writerow([i, FinalID, "Schoolfield Full", full[0]['B0'].value, full[0]['E'].value, \
			full[0]['E_l'].value, full[0]['T_l'].value, full[0]['E_h'].value, \
			full[0]['T_h'].value, "NA", "NA", "NA", "NA", full[1], full[2], full[3], full[4], full[5], full[6], Habitat])
		except:
			pass
		try:
			tl = schoolfield_model(data[data['id'] == i], parameterdata[parameterdata['id'] == i], model = "tl")

			results_csv.writerow([i, FinalID, "Schoolfield TL", tl[0]['B0'].value, tl[0]['E'].value, \
			tl[0]['E_l'].value, tl[0]['T_l'].value, "NA", "NA", "NA", "NA", "NA", "NA", \
			tl[1], tl[2], tl[3], tl[4], tl[5], tl[6], Habitat])
		except:
			pass
		try:
			th = schoolfield_model(data[data['id'] == i], parameterdata[parameterdata['id'] == i], model = "th")

			results_csv.writerow([i, FinalID, "Schoolfield TH", th[0]['B0'].value, th[0]['E'].value, \
			"NA", "NA", th[0]['E_h'].value, th[0]['T_h'].value, "NA", "NA", "NA", "NA", \
			th[1], th[2], th[3], th[4], th[5], th[6], Habitat])
		except:
			pass
		try:
			cubic = CubicModel(data[data['id'] == i])

			results_csv.writerow([i, FinalID, "Cubic", "NA", "NA", "NA", "NA", "NA", "NA", cubic[0]['c0'].value, \
			cubic[0]['c1'].value, cubic[0]['c2'].value, cubic[0]['c3'].value, cubic[1], cubic[2], cubic[3], \
			cubic[4], cubic[5], cubic[6], Habitat])
		except:
			pass
		## update progressbar
		pbar.update(i)

	## close progressbar and file, stop the timer
	pbar.finish()
	results.close()
	stop = timeit.default_timer()

	print("NLLS fitted! Time taken: " + str(stop - start) + " seconds. \n")

if __name__ == "__main__":
	#The input file name will be the minimum input, but you can add more inputs if you want
	# if(len(sys.argv) != 2):
	# 	print("Incorrect number of files chosen! Please supply in order a points file and a parameters file as arguments.")
	# else:
		main(sys.argv[1], sys.argv[2])
