#!/bin/bash
## Compiles the LaTeX document supplied as the argument.

## Get into the right directory
cd ../LaTeX/

## Compile the document
pdflatex $1.tex
pdflatex $1.tex
bibtex $1

## Do the word count
texcount -inc -incbib -sum -0 -template={SUM} writeup.tex | grep "[0-9]" > count.txt

## Finish compiling
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &

## Clean up temporary files
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.blg
rm *.bbl
