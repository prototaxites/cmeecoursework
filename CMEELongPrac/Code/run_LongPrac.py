#!/usr/bin/env python
"""Python script that runs the entire workflow, including document compilation.
     - Deletes old results files
     - Prepares the data in R
     - Fits the models in Python
     - Plots the models to graphs
     - Analyses the data
     - Compiles the LaTeX document
"""
__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.9'

import subprocess, os

## Remove old result files if they exist
subprocess.os.remove("../Data/params.csv") if os.path.isfile("../Data/params.csv") else None
subprocess.os.remove("../Data/ThermalResponse.csv") if os.path.isfile("../Data/ThermalResponse.csv") else None
subprocess.os.remove("../Results/results.csv")if os.path.isfile("../Results/results.csv") else None
subprocess.os.remove("../Results/E.pdf")if os.path.isfile("../Results/E.pdf") else None

## Run the scripts!
subprocess.os.system("Rscript data_analysis.R")
subprocess.os.system("python ThermalResponse.py ../Data/ThermalResponse.csv ../Data/params.csv")
## provide "parallel" as an argument to the following script to speed up graph creation, otherwse provide any other test as an argumentt
subprocess.os.system("Rscript data_plotting.R parallel")
subprocess.os.system("Rscript data_comparison.R")

## Build the LaTeX document
subprocess.os.system("bash compilelatex.sh writeup")
