Starting weekly assessment for James, Week3

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
PART 1: Checking project workflow...

Found the following directories in parent directory: .git, Week2, Week3, Week4, Assessment, CMEELongPrac, Week1, Week5, Week7, HPCPractical, Week8, Week6

Found the following files in parent directory: .gitignore~, README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~ 
*.tmp
*.pyc
*.tif
*.TIF
*.7z
*.tar.7z
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2015-16 Coursework Repository
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 8 weekly directories: Week1, Week2, Week3, Week4, Week5, Week6, Week7, Week8

The Week3 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK3...

Found the following directories: Practicals, Notes

Found the following files: README

Checking for readme file in weekly directory...

Found README in parent directory, named: README

Printing contents of README:
**********************************************************************
This folder contains the files from the GIS week of the course. Due
to their sheer size, I have excluded the .tif files from the data directory -
you can ask me for them if you want them. Otherwise, all other project files
are present, including shape files and points data.
**********************************************************************

Code directory missing!
Aborting this weeks assessment!

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 100

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!