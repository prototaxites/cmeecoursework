#1) Set working directory
setwd("~/Documents/CMEECourseWork/Week3/Practicals/Vegetation\ indices/")

#2) Install and load libraries you will need running your script
necessary.packages<-c("raster","reshape","sp","plotrix","utils","gtools","grDevices")
already.installed<-necessary.packages%in%installed.packages()[, 'Package']   

 if (length(necessary.packages[!already.installed])>=1) {    
    install.packages(necessary.packages[!already.installed],dep=T)     }
  sapply(necessary.packages,function(p) {require(p,quietly=T,character.only=T)})

#3) Read in image(s)
a<-stack("Day\ 1/LS2015_proj_clip.tif") #read the raster into R
a  #display raster properties

#Subset the rasters, which is a multi-stack object, to get specific image bands 
a.red<-subset(a,3,drop=TRUE) #get band 3
a.nir<-subset(a,4,drop=TRUE) #get band 4
a.blue<-subset(a,1,drop=TRUE) #get band 1

#4) Compute spectral vegetation indices such as NDVI
ndvi<-(a.nir-a.red)/(a.nir+a.red) #use band maths to create the ndvi raster
plot(ndvi) #plot the new raster

#5)  Save as new raster for read into QGIS
#give it a new name including the full path
filen<-paste("NDVI.tif",sep="/")
writeRaster(ndvi,filen,format="GTiff")
 #write the raster as geotiff into your working directory and gives it the name ‘NDVI.tif’

#6) Read in the table as csv: you should have copied it over
datnew<-read.csv("Day\1 /LAI_SouthAfrica_Pieter.csv",header=T) #
names(dat_new) #show the column headers of that file: the format should be a data frame in R
head(dat_new) #give the first lines of the data frame

#7) Extract plot coordinates from the table
pts<-data.frame(x=datnew$GPS_Long,y=datnew$GPS_Lat)
 #write the latitude and longitude columns into a new file
coordinates(pts)=~x+y #specify the projection 
proj4string(pts)=CRS("+init=epsg:4326") # set it to lat-long WS84 (the epsg code determines that)

#8) Match projection of raster to projection of plots
project.new<-projection(ndvi) #extract the projection from the NDVI raster into a text
pts.m = spTransform(pts,CRS(project.new)) 
#reproject the plots with the projection extracted from the ndvi raster
points(pts.m)#plot points on top of ndvi map
