#!/usr/bin/env python
"""sqlitemem.py - Using sqlite with databases stored in memory"""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import sqlite3

# Open a sqlite database in memory
conn = sqlite3.connect(":memory:")

# Connect to the database
c = conn.cursor()

# Add a table
c.execute("CREATE TABLE tt (Val TEXT)")

# Save
conn.commit()

# Vector of data
z = [('a',), ('ab',), ('abc',), ('b',), ('c',)]

# Add vector to table
c.executemany("INSERT INTO tt VALUES (?)", z)

# Save
conn.commit()

# print out all items in the database beginning with a
c.execute("SELECT * FROM tt WHERE Val LIKE 'a%'").fetchall()
conn.close()
