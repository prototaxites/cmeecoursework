#!/usr/bin/env python
"""using_os.py - Script that uses regular expressions to find files and
folders in home beginning with C or c that are returned from the
subprocess module."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

# Use the subprocess.os module to get a list of files and directories 
# Hint: look in subprocess.os and/or subprocess.os.path for helpful 
# functions

import subprocess
import re

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

## Compile a regular expression that finds strings starting with a C
regex = re.compile(r"^C.*?")

# Use a for loop to walk through the home directory.
for (root, dirs, files) in subprocess.os.walk(home):
    # Then loop through dirs and files to access them as strings rather than lists
    # and check for regex matches, which get stored
    for item in files:
        if regex.match(item):
            FilesDirsStartingWithC.append(item)
    for directory in dirs:
        if regex.match(directory):
            FilesDirsStartingWithC.append(directory)
            
print(FilesDirsStartingWithC)
            
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:

FilesDirsStartingWithCc = []

regex2 = re.compile(r"^[Cc].*?")
listofstuff = subprocess.os.walk(home)
for (root, dirs, files) in listofstuff:
    for item in files:
        if regex2.match(item):
            FilesDirsStartingWithCc.append(item)
    for directory in dirs:
        if regex2.match(directory):
            FilesDirsStartingWithCc.append(directory)
            
print(FilesDirsStartingWithCc)

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:

DirsStartingWithCc = []

for (root, dirs, files) in subprocess.os.walk(home):
    for directory in dirs:
        if regex2.match(directory):
            DirsStartingWithCc.append(directory)

print(DirsStartingWithCc)
