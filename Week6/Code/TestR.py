#!/usr/bin/env python
"""TestR.py - runs TestR.R and prints its output to the screen."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

# Open the script and print its output
subprocess.Popen("/usr/bin/Rscript --verbose TestR.R > ../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout", shell = True).wait()
