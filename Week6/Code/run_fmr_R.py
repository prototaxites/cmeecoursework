#!/usr/bin/python
"""run_fmr_R.py - script that executes fmr.R, prints its output to the screen
and checks its return code to see that it ran correctly"""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess
import os

## Grab the output of the R file through subprocess and Rscript
## stderr and stdout == subprocess.PIPE - send the outputs to Popen
## Shell = True - run as a shell command
## communicate() - save the information to P
P = subprocess.Popen("Rscript --verbose fmr.R", stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)

## Grab the stdout section of P and split it into a list of lines
routput = P.communicate()[0].splitlines()

## Loop through routput, print it and check if the last line of the script has run
for line in routput:
    print line

## Check that the script executed cleanly (return code is 0) and that the graph
## exists
if P.returncode == 0 and os.path.isfile("../Results/fmr_plot.pdf"):
        print "R script run successfully!"
        subprocess.os.system("evince ../Results/fmr_plot.pdf")
else:
    print "Errors encountered in R script."
