#!/usr/bin/env python
"""test_oaks.py - script debugged. Reads in a csv file of species names,
checks if they are oaks, and if so saves those species to a new csv file."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import csv
import sys
import pdb
import doctest

#Define function
def is_an_oak(name):
    """ Returns True if name starts with 'quercus '
    
        >>> is_an_oak('quercus')
        True

        >>> is_an_oak('quercuss')
        True

        >>> is_an_oak('fraxinus')
        False
    """

    ## Original check only checks if the string starts with quercus -
    ## so will capture "quercusasdsa" as well as "quercus"
    ## So instead capture an exact equivalence
    if name.lower() == "quercus":
    	return True
    else:
		return False
    
print(is_an_oak.__doc__)

def main(argv): 
    f = open('../Data/TestOaksData.csv','rb')
    g = open('../Data/JustOaksData.csv','wb')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa:
        print row
        print "The genus is", row[0]
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])    
    
    return 0
    
if (__name__ == "__main__"):
    status = main(sys.argv)

doctest.testmod()
