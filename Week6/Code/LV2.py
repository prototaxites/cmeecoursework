#!/usr/bin/env python
""" The Lotka-Volterra Model with carrying capacity simulated using scipy """

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import scipy as sc 
import scipy.integrate as integrate

import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dydt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dydt])

if len(sys.argv) == 6:
    r = float(sys.argv[1]) # Resource growth rate
    a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
    z = float(sys.argv[3]) # Consumer mortality rate
    e = float(sys.argv[4]) # Consumer production efficiency
    K = float(sys.argv[5]) # Carrying capacity
else:
	r = 1
	a = 0.1
	z = 1.5
	e = 0.75
	K = 100

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 150,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

params = "r = " + str(r) + "\na = " + str(a) + "\n" + "z = " + str(z) + "\ne = " + str(e) + "\nK = " + str(K)

prey, predators = pops.T # What's this for?
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-prey population dynamics')
p.figtext(0.805,0.12, params, style='italic', bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})
p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure
print "The final resource population is " + str(pops[99, 0])
print "The final consumer population is " + str(pops[99, 1])
