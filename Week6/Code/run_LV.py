# run_LV.py
"""run_LV.py - script that profiles each of the LVX.py scripts and prints
the results to screen."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

subprocess.os.system("python -m cProfile LV1.py")
subprocess.os.system("python -m cProfile LV2.py 1.5 0.1 0.5 0.75 20")
subprocess.os.system("python -m cProfile LV3.py 1.5 0.1 0.5 0.75 20")
subprocess.os.system("python -m cProfile LV4.py 1.5 0.1 0.5 0.75 20")
