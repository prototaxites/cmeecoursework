#!/usr/bin/env python
"""debugme.py - script that demonstrates the use of the ipdb debugger;
script attempts to divide by zero, but the debugger is called beforehand"""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

def createabug(x):
	y = x ** 4
	z = 0.
	#call debugger
	import ipdb; ipdb.set_trace()
	y = y/z
	return y

createabug(25)
