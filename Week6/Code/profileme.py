#!/usr/bin/python
"""profileme.py - Script with various slow functions; changing the function
range() to xrange() speeds them up."""

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

# Slow!
def a_useless_function(x):
    y = 0
    # eight zeros!
    for i in xrange(100000000):
        y= y + i
    return 0

# Less slow!
def a_less_useless_function(x):
    y = 0
    # five zeroes!
    for i in xrange(100000):
        y = y + 1
    return 0

def some_function(x):
    print x
    a_useless_function(x)
    a_less_useless_function(x)
    return 0

some_function(1000)
