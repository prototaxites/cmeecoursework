#!/usr/bin/env python
""" The discrete Lotka-Volterra Model simulated using scipy """

__author__ = 'Jim Downie (james.downie15@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import scipy as sc 
import scipy.integrate as integrate
import scipy.stats

import pylab as p #Contains matplotlib for plotting

def volterra_discrete(pops, t=0):
    """ Returns the size of predator and prey populations at any 
    given time step """

    R = pops[t, 0]
    C = pops[t, 1]
    Rnew = R*(1 + r*(1 - (R/K)) - a * C) 
    Cnew = C*(1 - z + e*a*R)
    
    pops[t+1, 0] = Rnew
    pops[t+1, 1] = Cnew

if len(sys.argv) == 6:
    r = float(sys.argv[1]) # Resource growth rate
    a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
    z = float(sys.argv[3]) # Consumer mortality rate
    e = float(sys.argv[4]) # Consumer production efficiency
    K = float(sys.argv[5]) # Carrying capacity
else:
	r = 1.5
	a = 0.1
	z = 0.5
	e = 0.75
	K = 20

# Definte the number of time steps
t = 100

# Initial starting values
resource_initial = 10
consumer_initial = 5

# Create an empty array of rows t, columns 2
# Assign initial values
pops = sc.zeros(shape = [t , 2], dtype = float)
pops[0,0] = resource_initial
pops[0,1] = consumer_initial

# Run volterra_discrete() for each time step
for step in range(t - 1):
	volterra_discrete(pops, step)

# Generate a string containing the parameters to print on the graph
params = "r = " + str(r) + "\na = " + str(a) + "\n" + "z = " + str(z) + "\ne = " + str(e) + "\nK = " + str(K)

prey, predators = pops.T # transpose the array into two lists
f1 = p.figure() #Open empty figure object
p.plot(range(t), prey, 'g-', label='Resource density') # Plot
p.plot(range(t), predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-prey population dynamics')
# Add legend with parameter values
p.figtext(0.805,0.12, params, style='italic', bbox={'facecolor':'red', 'alpha':0.5, 'pad':10})
p.show()
f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure
print "The final resource population is " + str(pops[t - 1, 0])
print "The final consumer population is " + str(pops[t - 1, 1])
