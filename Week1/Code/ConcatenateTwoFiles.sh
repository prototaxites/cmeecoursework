#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: ConcatenateTwoFiles.sh
# Desc: Merge two files ($1 and $2) together into a third
# Aruments: none
# Date: Oct 2015

# Write file 1 to file 3
cat $1 > $3

# Append file 2 to the end of file 3
cat $2 >> $3
echo "Merged files."

# Print of file 3
cat $3
