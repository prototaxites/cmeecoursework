#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: CompileLaTex.sh
# Desc: Runs the necessary commands to compile a LaTeX+BibTeX document, then cleans up temporary files.
# Aruments: none
# Date: Oct 2015

# Create a pdf from the specified LaTeX base files
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
# Open the new pdf
evince $1.pdf &

##cleanup
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
