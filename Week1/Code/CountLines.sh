#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: Countlines.sh
# Desc: Counts the number of lines in a file.
# Aruments: none
# Date: Oct 2015

NumLines=`wc -l < $1` #count the number of lines in file $1
echo "The file $1 has $NumLines lines."
