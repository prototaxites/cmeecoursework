#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: Boilerplate.sh
# Desc: simple boilerplate for shell scripts - prints 'this is a shell script!'
# Aruments: none
# Date: Oct 2015

echo -e "\nThis is a shell script! \n"

#exit
