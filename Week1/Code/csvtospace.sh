#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: csvtospace.sh
# Desc: Converts all commas to spaces in a file
# Aruments: none
# Date: Oct 2015

# Check if argument is supplied, if not then print error and quit
if [ -z "$1" ];
	then
	echo "No file supplied."
	exit 1
# If argument is supplied, convert all commas to spaces and save as a new file with extension .ssv
else
	echo "Converting commas to spaces..."
	cat $1 | tr -s "," " " > $1.ssv
	echo "Done! \o/"
fi
