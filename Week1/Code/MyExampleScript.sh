#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: MyExampleScript.sh
# Desc: Prints the message "Hello user" in two different ways
# Aruments: none
# Date: Oct 2015

msg1=hello
msg2=$USER
echo "$msg1 $msg2"
echo "Hello $USER"
