#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute tabs in files with commas, outputs .csv
# Aruments: 1 -> tab delimited file
# Date: Oct 2015

# Check if argument is supplied, if not then print error and quit
if [ -z "$1" ];
	then
	echo "No file given."
	exit 1
# If argument is supplied, convert all tabs to commas and save as a new file with extension .csv
else
	echo "Creating CSV of $1..."
	cat $1 | tr -s "\t" "," > $1.csv #replace tabs with spaces
	echo "\o/"
fi

exit
