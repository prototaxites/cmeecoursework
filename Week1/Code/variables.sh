#!/bin/bash
# Author: Jim Downie, james.downie15@imperial.ac.uk
# Script: Countlines.sh
# Desc: Shows the use of variables - using them in commands, and reading them in on the fly.
# Aruments: none
# Date: Oct 2015

MyVar='some string'
echo "The current value of MyVar is " $MyVar
echo "Please enter a new string"
read MyVar #reads in a value for variable MyVar
echo "The current value of MyVar is: 	\"" $MyVar "\""
echo "Enter two values separated by spaces:"
read a b #reads in values for two variables, a and b
mysum=`expr $a + $b`
echo "You entered " $a "and " $b ". Their sum is:" $mysum
