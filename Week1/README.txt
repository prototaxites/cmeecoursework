This directory contains all the relevant code and data files for Week 1
of the CMEE course. The file structure is split amongst 3 directories,
Code, Data and Sandbox. The files are as follows (output of tree command)

.
├── Code
│   ├── boilerplate.sh # Prints "This is a shell script!"
│   ├── ConcatenateTwoFiles.sh # Merges two files together into a third
│   ├── CountLines.sh # Counts the number of lines in a file.
│   ├── csvtospace.sh # Converts all commas to spaces in a file
│   ├── Latex
│   │   ├── CompileLaTeX.sh # Runs the necessary commands to compile a LaTeX+BibTeX document, then cleans up temporary files.
│   │   ├── FirstBiblio.bib
│   │   ├── FirstExample.bbl
│   │   ├── FirstExample.blg
│   │   ├── FirstExample.pdf
│   │   └── FirstExample.tex
│   ├── MyExampleScript.sh # Prints the message "Hello user" in two different ways
│   ├── tabtocsv.sh # Substitutes tabs in files with commas, outputs as a .csv
│   ├── UnixPrac1.txt # Text file with the requisite commands to perform various operations on DNA sequences of E.coli
│   └── variables.sh # Shows how to use/read variables
├── Data
│   ├── 1800.csv
│   ├── 1800.csv.ssv
│   ├── 1801.csv
│   ├── 1801.csv.ssv
│   ├── 1802.csv
│   ├── 1802.csv.ssv
│   ├── 1803.csv
│   ├── 1803.csv.ssv
│   ├── 407228326.fasta
│   ├── 407228412.fasta
│   ├── E.coli.fasta
│   └── spawannxs.txt
├── README.txt
└── Sandbox
    ├── ListRootDir.txt
    ├── test
    ├── test1
    ├── test2
    ├── test3
    ├── TestFind
    │   ├── Dir1
    │   │   ├── Dir11
    │   │   │   └── Dir111
    │   │   │       └── File111.txt
    │   │   ├── File1.csv
    │   │   ├── File1.tex
    │   │   └── File1.txt
    │   ├── Dir2
    │   │   ├── file2.csv
    │   │   ├── File2.tex
    │   │   └── File2.txt
    │   └── Dir3
    │       └── File3.txt
    ├── test.txt
    ├── test.txt.csv
    └── TestWild
        ├── Anotherfile.csv
        ├── Anotherfile.txt
        ├── File1.csv
        ├── File1.txt
        ├── File2.csv
        ├── File2.txt
        ├── File3.csv
        ├── File3.txt
        ├── File4.csv
        └── File4.txt

11 directories, 53 files
