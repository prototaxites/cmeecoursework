#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l select=1:ncpus=1:mem=800mb
module load R/2.13.0
module load intel-suite
echo "R is about to run"
R --vanilla < $WORK/Code/jd1915_cluster.R
mv nmSim* $WORK/Results
echo "R has finished running"
#this is a comment at the end of the file
