## TAutoCorr.R

## Load in requisite libraries and data
library(ggplot2)
load("../Data/KeyWestAnnualMeanTemperature.RData")

## Initialise a new vector and timeshift the temperature data by 1
## so we can correlate a year's temperature with the succeeding year's temperature
newtemp = vector()
for(i in 1:100){
  newtemp[i] <- ats$Temp[(i+1)]
}

## Get a correlation coefficient for temperature against the subsequent year's temperature
firstcor <- cor(ats$Temp[1:99],newtemp[1:99])

## Initialise a couple of vectors
j <- vector()
k <- vector()

## Permute the data
## For every permutation, pull out 50 pairs of random temperature numbers
## Sort these numbers into a matrix, then run a correlation on the matrix
## columns. Save each correlation coefficient to vector j.
for(i in 1:10000) {
  reps <- replicate(50, sample(ats$Temp, size = 2, replace = F))
  reps <- matrix(reps, nrow = 50, ncol = 2, byrow = T)
  j[i] <- cor(reps[,1],reps[,2])
}

## Compute a fraction of the permuted correlation coefficients above the
## coefficient for the actual data
for(i in 1:length(j)){
  if(j[i] >= firstcor) {
    k[i] <- j[i]
  }
}
percentage <- 100-((length(k)/length(j))*100)
print(paste("Percentage larger:", percentage))

## Plot a pretty density curve!
plot <- ggplot(data.frame(a = j), aes(x = a)) + geom_density() + 
        geom_vline(x = firstcor, colour = "red", linetype = "longdash") +
        xlab("Permuted correlation coefficient") + ylab("Density") +
        theme_classic()
print(plot)