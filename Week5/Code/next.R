## next.R
## Demonstrates the use of next to pass on to the next iteration of a loop

for (i in 1:10) {
  if ((i %% 2) == 0)
    next # pass to next iteration of loop
  print(i)
}
