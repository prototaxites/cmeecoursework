Week 5 practicals - Biological computing in R week. The files are split 
amongst 3 directories - Code, Data, and Results. Code contains the various
R/bash script files for this week, Data contains the referenced data/csv
files in the R scripts, and Results contains the various saved graphs
and analyses from the scripts.
.
├── Code
│   ├── apply.R                            - Demonstrates the use of apply functions on a matrix
│   ├── basic_io.R                         - Demonstrates te basics of reading and writing files in R
│   ├── break.R                            - Shows the use of the break frunction to escape loops
│   ├── browse.R                           - Plots a simple graph of exponential growth, and shows how to enter the browser to debug code
│   ├── control.R                          - Various control flows in R
│   ├── get_TreeHeight.R                   - Takes in a csv file as an argument and returns a vector of heights calculated from degrees and distances, and saves the vector to a new file
│   ├── Latex                              - Folder containing LaTeX code for the TAutoCorr.R practical
│   │   ├── figure
│   │   │   └── unnamed-chunk-2-1.pdf
│   │   ├── pdf-knitr-concordance.tex
│   │   ├── pdf-knitr.log
│   │   ├── pdf-knitr.Rnw                  - Knittr/LaTeX script
│   │   ├── pdf-knitr.synctex.gz
│   │   └── pdf-knitr.tex
│   ├── maps.R                             - Simple script that plots some points onto a world map
│   ├── next.R                             - Simple script demonstating the use of next inside loops to move to the next iteration
│   ├── PP_Lattice.R                       - Plots some faceted graphs of predator and prey mass delineated by feeding type
│   ├── PP_Regress_loc.R                   - Script that plots a series of graphs of predator and prey mass faceted by feeding type and location, and runs a series of linear models to save the intercepts, slopes, p values etc. of each of the lines in the graph to a dataframe
│   ├── PP_Regress.R                       - Script that plots a series of graphs of predator and prey mass faceted by feeding type, and runs a series of linear models to save the intercepts, slopes, p values etc. of each of the lines in the graph to a dataframe
│   ├── Ricker.R                           - Runs and plots a simple, non-stochastic Ricker model
│   ├── run_get_TreeHeight.sh              - Bash script that runs get_TreeHeight.R with the correct arguments
│   ├── sample.R                           - Samples a random population and generates means
│   ├── TAutoCorr.R                        - Runs permutations on a timeseries of temperature data, to determine whether the correlation found is better than chance.
│   ├── TreeHeight.R                       - Script that writes a csv file of tree heights calculated as a function of distance from the tree and angle of elevation
│   ├── try.R                              - Demonstrates the use of try in R
│   ├── Vectorize1.R                       - Simple script that demonstrates how to sum a matrix using for loops
│   └── Vectorize2.R                       - Script that runs a stochastic Ricker model and a vectorized stochastic Ricker model and compares their run speeds
├── Data
│   ├── EcolArchives-E089-51-D1.csv
│   ├── GPDDFiltered.RData
│   ├── KeyWestAnnualMeanTemperature.RData
│   └── trees.csv
├── README
└── Results
    ├── MyData.csv                         
    ├── MyFirst-ggplot2-Figure.pdf
    ├── PP_Regress_Feeding_Type.pdf
    ├── PP_Regress_Location.pdf
    ├── PP_Regress_Results.csv
    ├── PP_Regress_Results_Location.csv
    ├── PP_Results.csv
    ├── Pred_Lattice.pdf
    ├── Pred_Prey_Overlay.pdf
    ├── Prey_Lattice.pdf
    ├── SizeRatio_Lattice.pdf
    ├── TemperaturePermutations.pdf       - PDF containing the graph and analysis of TAutoCorr.R
    ├── TreeHts.csv
    └── trees.csv_treeheights.csv

5 directories, 44 files
